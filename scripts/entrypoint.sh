#!/bin/sh

set -e

python manage.py collectstatic --noinput
#python manage.py wait_for_db
python manage.py migrate

#uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
#daphne --bind 9000  app.asgi:application
daphne --bind 0.0.0.0 --port 8000 app.asgi:application