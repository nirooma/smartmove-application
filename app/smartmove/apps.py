from django.apps import AppConfig


class SmartmoveConfig(AppConfig):
    name = 'smartmove'
